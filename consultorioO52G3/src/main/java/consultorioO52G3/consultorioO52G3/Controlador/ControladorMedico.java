
package consultorioO52G3.consultorioO52G3.Controlador;

import consultorioO52G3.consultorioO52G3.Modelos.Medico;
import consultorioO52G3.consultorioO52G3.Servicio.ServicioMedico;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author varga
 */

@RestController
@CrossOrigin("*")
@RequestMapping("/Medico")
public class ControladorMedico {
    
    
     @Autowired
    private ServicioMedico serviceMedico;

    @GetMapping("/listamedico")
    public List<Medico> consultarTodo() {
        return serviceMedico.findAll();
    }

    @PostMapping("/insertarmedico")
    public Medico insertar(@RequestBody Medico med) {
        return serviceMedico.insertar(med);
    }

    @PutMapping("/actualizarmedico")
    public Medico actualizar(@RequestBody Medico med) {
        return serviceMedico.actualizar(med);
    }
    
    
    @DeleteMapping(path = "/{idMedico}")
    public String eliminar(@PathVariable Integer idMedico) {
        try {
            serviceMedico.eliminarMedico(idMedico);
            return "Eliminado Exitosamente";
        } catch (Exception e) {
            return "Error al Eliminar " + e.getMessage();
        }
    }

    
    @GetMapping("/buscarbyname")
    public List<Medico> buscarPorNombre(@RequestParam String nombres) {
        try {
            return serviceMedico.buscarPorNombre(nombres);
        } catch (Exception e) {
            return null;
        }
    }
    
    @GetMapping("/query")
    public List<Medico> buscarPorConsulta(@RequestParam String identificacion) {
        try {
            return serviceMedico.buscarPorIdentificacion(identificacion);
        } catch (Exception e) {
            return null;
        }
    }
}
