
package consultorioO52G3.consultorioO52G3.Controlador;

import consultorioO52G3.consultorioO52G3.Modelos.Cita;
import consultorioO52G3.consultorioO52G3.Servicio.ServicioCita;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author varga
 */

@RestController
@CrossOrigin("*")
@RequestMapping("/Cita")
public class ControladorCita {
    
       @Autowired
    private ServicioCita servicioCita;
    
    @GetMapping("/listacita")
    public List<Cita> MostrarTodo(){
        return servicioCita.mostrarTodo();
    }
    
    @GetMapping("/id")
    public Cita getporId(int idCita){
        Cita citas =servicioCita.buscarPorId(idCita);
        return citas;
    }
    
    @PostMapping("/insertarcita")
    public Cita insertar(@RequestBody Cita ncit){
        return servicioCita.insertar(ncit);
    }
    @PutMapping("/actualizarcita")
    public Cita actualizar(@RequestBody Cita ncit){
        return servicioCita.actualizar(ncit);
    }
    
    @DeleteMapping(path = "/{idCita}")
    public String eliminar(@PathVariable Integer idCita){
        try {
            servicioCita.eliminar(idCita);
            return "Eliminada Exitosamente";
        } catch (Exception e) {
            return "error al eliminar";
        }
    }
    
    @GetMapping("/buscar")
    public List<Cita> buscarPorNombrePaciente(Integer idPaciente){
        return servicioCita.buscarPorNombrePaciente(idPaciente);
    }
    
}
