
package consultorioO52G3.consultorioO52G3.Controlador;


import consultorioO52G3.consultorioO52G3.Modelos.Acceso;
import consultorioO52G3.consultorioO52G3.Servicio.ServicioAcceso;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;


@RestController
@CrossOrigin("*")
@RequestMapping("/acceso")
public class ControladorAcceso {
    @Autowired
    private ServicioAcceso servicio;
    
    @GetMapping
    public Acceso login(String email,String clave){
        return servicio.login(email, clave);
    }
    

    
}
