
package consultorioO52G3.consultorioO52G3.Controlador;

import consultorioO52G3.consultorioO52G3.Modelos.Paciente;
import consultorioO52G3.consultorioO52G3.Servicio.ServicioPaciente;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author varga
 */
@RestController
@CrossOrigin("*")
@RequestMapping("/paciente")
public class ControladorPaciente {
     @Autowired
    private ServicioPaciente servicePaciente;

    @GetMapping("/listapaciente")
    public List<Paciente> consultarTodo() {
        return servicePaciente.findAll();
    }

    @PostMapping("/insertarpaciente")
    public Paciente insertar(@RequestBody Paciente pac) {
        return servicePaciente.insertar(pac);
    }

    @PutMapping("/updatepaciente")
    public Paciente actualizar(@RequestBody Paciente pac) {
        return servicePaciente.actualizar(pac);
    }

    @DeleteMapping(path = "/{idPaciente}")
    public String eliminar(@PathVariable Integer idPaciente) {
        try {
            servicePaciente.eliminarPaciente(idPaciente);
            return "Eliminado Exitosamente";
        } catch (Exception e) {
            return "Error al Eliminar " + e.getMessage();
        }
    }

    
    @GetMapping("/buscarbyname")
    public List<Paciente> buscarPorNombre(@RequestParam String nombres) {
        try {
            return servicePaciente.buscarPorNombre(nombres);
        } catch (Exception e) {
            return null;
        }
    }
    
    @GetMapping("/query")
    public List<Paciente> buscarPorConsulta(@RequestParam String identificacion) {
        try {
            return servicePaciente.buscarPorIdentificacion(identificacion);
        } catch (Exception e) {
            return null;
        }
    }
}
