
package consultorioO52G3.consultorioO52G3.Controlador;

import consultorioO52G3.consultorioO52G3.Dao.DaoHorario;
import consultorioO52G3.consultorioO52G3.Modelos.Horario;
import consultorioO52G3.consultorioO52G3.Servicio.ServicioHorario;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author varga
 */
@RestController
@CrossOrigin("*")
@RequestMapping("/horario")
public class ControladorHorario {
    
    @Autowired
    private ServicioHorario ServicioHorario;
    
       @GetMapping("/listahorario")
    public List<Horario> MostrarDetalles(){
        return ServicioHorario.mostrarTodo();
    }
    
    @GetMapping("/idhorario")
       public List<Horario> BuscarHorarioMedico(Integer idMedico) {
        return  ServicioHorario.buscarHorarioIdMedico(idMedico);
}
}
