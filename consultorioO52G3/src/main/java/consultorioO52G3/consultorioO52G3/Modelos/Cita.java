/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package consultorioO52G3.consultorioO52G3.Modelos;

import com.fasterxml.jackson.annotation.JsonFormat;
import java.io.Serializable;
import java.time.LocalDateTime;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Table
@Entity(name="cita")
public class Cita implements Serializable {

@Id
@GeneratedValue(strategy = GenerationType.IDENTITY)
@Column(name="idcita")
private Integer idCita;

@ManyToOne
@JoinColumn(name="idpaciente")
private Paciente paciente;

@ManyToOne
@JoinColumn(name="idmedico")
private Medico medico;

@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
@Column(name="fechasolicitud")
private LocalDateTime FechaSolicitud;

@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
@Column(name="fechaasignada")
private LocalDateTime FechaAsignada;

@Column(name="estadoafiliacion")
private String EstadoAfiliacion;

@Column(name="estadocita")
private String EstadoCita;

@Column(name="observacion")
private String Observacion;

@Column(name="formulamedica")
private String FormulaMedica;

    public Integer getIdCita() {
        return idCita;
    }

    public void setIdCita(Integer idCita) {
        this.idCita = idCita;
    }

    public Paciente getPaciente() {
        return paciente;
    }

    public void setPaciente(Paciente paciente) {
        this.paciente = paciente;
    }

    public Medico getMedico() {
        return medico;
    }

    public void setMedico(Medico medico) {
        this.medico = medico;
    }

    public LocalDateTime getFechaSolicitud() {
        return FechaSolicitud;
    }

    public void setFechaSolicitud(LocalDateTime FechaSolicitud) {
        this.FechaSolicitud = FechaSolicitud;
    }

    public LocalDateTime getFechaAsignada() {
        return FechaAsignada;
    }

    public void setFechaAsignada(LocalDateTime FechaAsignada) {
        this.FechaAsignada = FechaAsignada;
    }

    public String getEstadoAfiliacion() {
        return EstadoAfiliacion;
    }

    public void setEstadoAfiliacion(String EstadoAfiliacion) {
        this.EstadoAfiliacion = EstadoAfiliacion;
    }

    public String getEstadoCita() {
        return EstadoCita;
    }

    public void setEstadoCita(String EstadoCita) {
        this.EstadoCita = EstadoCita;
    }

    public String getObservacion() {
        return Observacion;
    }

    public void setObservacion(String Observacion) {
        this.Observacion = Observacion;
    }

    public String getFormulaMedica() {
        return FormulaMedica;
    }

    public void setFormulaMedica(String FormulaMedica) {
        this.FormulaMedica = FormulaMedica;
    }



}