package consultorioO52G3.consultorioO52G3.Modelos;

import com.fasterxml.jackson.annotation.JsonFormat;
import java.io.Serializable;
import java.time.LocalDate;
import java.time.LocalDateTime;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Table
@Entity(name = "paciente")
public class Paciente implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "idpaciente")
    private Integer idPaciente;

    @Column(name = "identificacion")
    private String Identificacion;

    @JsonFormat(pattern = "yyyy-MM-dd")
    @Column(name = "fechaexpedicion")
    private LocalDate FechaExpedicion;

    @Column(name = "nombres")
    private String Nombres;

    @Column(name = "apellidos")
    private String Apellidos;

    @JsonFormat(pattern = "yyyy-MM-dd")
    @Column(name = "fechanacimiento")
    private LocalDate FechaNacimiento;

    @Column(name = "codhistoriaclinica")
    private String CodHistoriaClinica;

    @Column(name = "numerocontacto")
    private Integer NumeroContacto;

    @Column(name = "direccioncontacto")
    private String DireccionContacto;

    @Column(name = "estadoafiliacion")
    private String EstadoAfiliacion;

    public Integer getIdPaciente() {
        return idPaciente;
    }

    public void setIdPaciente(Integer idPaciente) {
        this.idPaciente = idPaciente;
    }

    public String getIdentificacion() {
        return Identificacion;
    }

    public void setIdentificacion(String Identificacion) {
        this.Identificacion = Identificacion;
    }

    public LocalDate getFechaExpedicion() {
        return FechaExpedicion;
    }

    public void setFechaExpedicion(LocalDate FechaExpedicion) {
        this.FechaExpedicion = FechaExpedicion;
    }

    public String getNombres() {
        return Nombres;
    }

    public void setNombres(String Nombres) {
        this.Nombres = Nombres;
    }

    public String getApellidos() {
        return Apellidos;
    }

    public void setApellidos(String Apellidos) {
        this.Apellidos = Apellidos;
    }

    public LocalDate getFechaNacimiento() {
        return FechaNacimiento;
    }

    public void setFechaNacimiento(LocalDate FechaNacimiento) {
        this.FechaNacimiento = FechaNacimiento;
    }

    public String getCodHistoriaClinica() {
        return CodHistoriaClinica;
    }

    public void setCodHistoriaClinica(String CodHistoriaClinica) {
        this.CodHistoriaClinica = CodHistoriaClinica;
    }

    public Integer getNumeroContacto() {
        return NumeroContacto;
    }

    public void setNumeroContacto(Integer NumeroContacto) {
        this.NumeroContacto = NumeroContacto;
    }

    public String getDireccionContacto() {
        return DireccionContacto;
    }

    public void setDireccionContacto(String DireccionContacto) {
        this.DireccionContacto = DireccionContacto;
    }

    public String getEstadoAfiliacion() {
        return EstadoAfiliacion;
    }

    public void setEstadoAfiliacion(String EstadoAfiliacion) {
        this.EstadoAfiliacion = EstadoAfiliacion;
    }

}
