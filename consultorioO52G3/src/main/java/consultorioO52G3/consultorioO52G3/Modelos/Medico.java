
package consultorioO52G3.consultorioO52G3.Modelos;


import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Table
@Entity(name = "medico")

public class Medico implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name="idmedico")
    private Integer idMedico;

    @Column(name="identificacion")
    private String Identificacion;

    @Column(name="nombres")
    private String Nombres;

    @Column(name="apellidos")
    private String Apellidos;

    @Column(name="especialidad")
    private String Especialidad;

    @Column(name="numerocontacto")
    private String NumeroContacto;

    public Integer getIdMedico() {
        return idMedico;
    }

    public void setIdMedico(Integer idMedico) {
        this.idMedico = idMedico;
    }

    public String getIdentificacion() {
        return Identificacion;
    }

    public void setIdentificacion(String Identificacion) {
        this.Identificacion = Identificacion;
    }

    public String getNombres() {
        return Nombres;
    }

    public void setNombres(String Nombres) {
        this.Nombres = Nombres;
    }

    public String getApellidos() {
        return Apellidos;
    }

    public void setApellidos(String Apellidos) {
        this.Apellidos = Apellidos;
    }

    public String getEspecialidad() {
        return Especialidad;
    }

    public void setEspecialidad(String Especialidad) {
        this.Especialidad = Especialidad;
    }

    public String getNumero_Contacto() {
        return NumeroContacto;
    }

    public void setNumero_Contacto(String Numero_Contacto) {
        this.NumeroContacto = Numero_Contacto;
    }

}