
package consultorioO52G3.consultorioO52G3.Modelos;

import com.fasterxml.jackson.annotation.JsonFormat;
import java.io.Serializable;
import java.time.LocalDateTime;
import java.time.LocalTime;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;


@Table
@Entity(name="horario")
public class Horario implements Serializable{
    
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name="idhorario")
    private Integer idHorario;
    
    @ManyToOne
    @JoinColumn(name="idmedico")
    private Medico medico;
    
    @JsonFormat(pattern = "HH:mm:ss")
    @Column(name="iniciojornada")
    private LocalTime InicioJornada;
    
    @JsonFormat(pattern = "HH:mm:ss")
    @Column(name="finjornada")
    private LocalDateTime FinJornada;

    public Integer getIdHorario() {
        return idHorario;
    }

    public void setIdHorario(Integer idHorario) {
        this.idHorario = idHorario;
    }

    public Medico getMedico() {
        return medico;
    }

    public void setMedico(Medico medico) {
        this.medico = medico;
    }

    public LocalTime getInicioJornada() {
        return InicioJornada;
    }

    public void setInicioJornada(LocalTime InicioJornada) {
        this.InicioJornada = InicioJornada;
    }

    public LocalDateTime getFinJornada() {
        return FinJornada;
    }

    public void setFinJornada(LocalDateTime FinJornada) {
        this.FinJornada = FinJornada;
    }

  
    
}
