/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Interface.java to edit this template
 */
package consultorioO52G3.consultorioO52G3.Dao;

import consultorioO52G3.consultorioO52G3.Modelos.Medico;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

/**
 *
 * @author varga
 */
public interface DaoMedico extends JpaRepository<Medico, Integer>{
      //Buscar medicos identificacion
    @Query(value="select * from medico where identificacion=:identificacion",nativeQuery = true)
    List<Medico> BuscarPoridentificacion(@Param("identificacion")String identificacion);
    
    //Buscar medicos que contengan una parte del nombre
     @Query(value="select * from medico where nombres=:nombres",nativeQuery = true)
    List<Medico> findBynombreMedicoContains(String nombres);
    
    
    //Buscar Atravez de una Consulta
    @Query(value="SELECT * FROM medico m WHERE m.idmedico =:parametro",nativeQuery = true)
    List<Medico> searchByIdMedicoStartsWith(@Param("parametro") Integer idMedico);
    
}
