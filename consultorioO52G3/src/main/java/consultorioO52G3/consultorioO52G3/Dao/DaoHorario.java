/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Interface.java to edit this template
 */
package consultorioO52G3.consultorioO52G3.Dao;

import consultorioO52G3.consultorioO52G3.Modelos.Cita;
import consultorioO52G3.consultorioO52G3.Modelos.Horario;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

/**
 *
 * @author varga
 */
public interface DaoHorario extends JpaRepository<Horario, Integer>{
    
    @Query(value="select h.* from horario h inner join medico m on m.idmedico=h.idhorario  = h.idhorario WHERE m.idmedico = :idMedico",nativeQuery = true)
    List<Horario> searchByidMedico(@Param("idMedico") Integer idMedico);
}
