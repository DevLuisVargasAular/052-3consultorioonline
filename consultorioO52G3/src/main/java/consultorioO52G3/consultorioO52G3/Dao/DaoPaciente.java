
package consultorioO52G3.consultorioO52G3.Dao;

import consultorioO52G3.consultorioO52G3.Modelos.Paciente;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

/**
 *
 * @author varga
 */
public interface DaoPaciente extends JpaRepository<Paciente, Integer>{
    
       //Buscar paciente identificacion
    @Query(value="select * from paciente where identificacion=:identificacion",nativeQuery = true)
    List<Paciente> BuscarPorIdentificacionPaciente(String identificacion);
    
    //Buscar paciente que contengan una parte del nombre
    @Query(value="select * from paciente where nombres=:nombres",nativeQuery = true)
    List<Paciente> findBynombrePacienteContains(String nombres);
    
    
    //Buscar Atravez de una Consulta
    @Query(value="SELECT * FROM paciente p WHERE p.idpaciente =:parametro",nativeQuery = true)
    List<Paciente> searchByIdPacienteStartsWith(@Param("parametro") Integer idPaciente);
    
}
