/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package consultorioO52G3.consultorioO52G3.Dao;


import consultorioO52G3.consultorioO52G3.Modelos.Acceso;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

/**
 *
 * @author MEC
 */
public interface DaoAcceso extends JpaRepository<Acceso,Integer> {
    
    @Query(value="SELECT * FROM acceso where email=:email and clave=:clave ",nativeQuery=true)
    Acceso login(@Param("email") String email,@Param("clave") String clave);
    
    
}
