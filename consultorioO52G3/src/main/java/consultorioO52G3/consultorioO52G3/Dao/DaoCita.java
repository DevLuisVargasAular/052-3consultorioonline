/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Interface.java to edit this template
 */
package consultorioO52G3.consultorioO52G3.Dao;

import consultorioO52G3.consultorioO52G3.Modelos.Cita;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

/**
 *
 * @author varga
 */
public interface DaoCita extends JpaRepository<Cita, Integer>{
   
 @Query(value="select c.* from cita c inner join paciente p on p.idpaciente=c.idpaciente inner join medico m on m.idmedico = c.idmedico WHERE p.idpaciente = :idPaciente",nativeQuery = true)
    List<Cita> searchByidPaciente(@Param("idPaciente") Integer idPaciente);
}
