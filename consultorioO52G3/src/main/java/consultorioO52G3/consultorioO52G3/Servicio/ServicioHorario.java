/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package consultorioO52G3.consultorioO52G3.Servicio;


import consultorioO52G3.consultorioO52G3.Dao.DaoHorario;

import consultorioO52G3.consultorioO52G3.Modelos.Horario;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestParam;

/**
 *
 * @author varga
 */
@Service
public class ServicioHorario {
     @Autowired 
    private DaoHorario daoHorario;
    
     public List<Horario> mostrarTodo(){
        return daoHorario.findAll();
        
     
    }
    public List<Horario> buscarHorarioIdMedico(@RequestParam Integer idMedico){
        return daoHorario.searchByidMedico(idMedico);
    } 
    
}
