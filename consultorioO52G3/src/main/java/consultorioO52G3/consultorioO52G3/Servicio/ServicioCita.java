/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package consultorioO52G3.consultorioO52G3.Servicio;

import consultorioO52G3.consultorioO52G3.Dao.DaoCita;
import consultorioO52G3.consultorioO52G3.Modelos.Cita;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestParam;

/**
 *
 * @author varga
 */
@Service
public class ServicioCita {
    
     @Autowired
    private DaoCita daoCita;
     
     public Cita insertar(Cita ncit){
        return daoCita.save(ncit);
    }
    
    public Cita guardarCita(Cita ncit){
        return daoCita.save(ncit);
    }
     public Cita actualizar(Cita ncit){
        return daoCita.save(ncit);
    }
    public List<Cita> mostrarTodo(){
        return daoCita.findAll();
    }
    
     public Cita buscarPorId(int idCita){
        return daoCita.findById(idCita).get();
    }
    
    public void eliminar(int idCita){
        daoCita.delete(daoCita.findById(idCita).get());
    }
    
    public List<Cita> buscarPorNombrePaciente(@RequestParam Integer idPaciente){
        return daoCita.searchByidPaciente(idPaciente);
    } 

   
    
}
