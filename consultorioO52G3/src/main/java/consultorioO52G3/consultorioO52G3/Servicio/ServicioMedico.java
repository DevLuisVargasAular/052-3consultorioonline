/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package consultorioO52G3.consultorioO52G3.Servicio;

import consultorioO52G3.consultorioO52G3.Dao.DaoMedico;
import consultorioO52G3.consultorioO52G3.Modelos.Medico;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author varga
 */
@Service
public class ServicioMedico {
       @Autowired
    private DaoMedico DaoMedico ;
    
    public Medico insertar(Medico med){
        return DaoMedico.save(med);
    }
    
    public Medico actualizar(Medico med){
        return DaoMedico.save(med);
    }
    
    public List<Medico> findAll() {
        return (List<Medico>) DaoMedico.findAll();
    }
 
    
    public void eliminarMedico(int idMedico){
        DaoMedico.delete(DaoMedico.findById(idMedico).get());
    }
    
    
    public List<Medico> buscarPorIdentificacion(String identificacion){
        return DaoMedico.BuscarPoridentificacion(identificacion);
    }
    
    public List<Medico> buscarPorNombre(String nombres){
        return DaoMedico.findBynombreMedicoContains(nombres);
    }
    
    public List<Medico> buscarPorConsulta(Integer idMedico){
        return DaoMedico.searchByIdMedicoStartsWith(idMedico);
    }
    
  
    
}
