
package consultorioO52G3.consultorioO52G3.Servicio;


import consultorioO52G3.consultorioO52G3.Dao.DaoAcceso;
import consultorioO52G3.consultorioO52G3.Modelos.Acceso;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


@Service
public class ServicioAcceso {
    @Autowired 
    private DaoAcceso dao;
    
    public Acceso login(String usuario,String clave){
        return dao.login(usuario, clave);
    }
    
}
