/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package consultorioO52G3.consultorioO52G3.Servicio;

import consultorioO52G3.consultorioO52G3.Dao.DaoPaciente;
import consultorioO52G3.consultorioO52G3.Modelos.Paciente;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author varga
 */
@Service
public class ServicioPaciente {

    @Autowired
    private DaoPaciente DaoPaciente;

    public Paciente insertar(Paciente pac) {
        return DaoPaciente.save(pac);
    }

    public Paciente actualizar(Paciente pac) {
        return DaoPaciente.save(pac);
    }

    public List<Paciente> findAll() {
        return (List<Paciente>) DaoPaciente.findAll();
    }

    public void eliminarPaciente(int idPaciente) {
        DaoPaciente.delete(DaoPaciente.findById(idPaciente).get());
    }


    public List<Paciente> buscarPorIdentificacion(String identificacion) {
        return DaoPaciente.BuscarPorIdentificacionPaciente(identificacion);
    }

    public List<Paciente> buscarPorNombre(String nombres) {
        return DaoPaciente.findBynombrePacienteContains(nombres);
    }

    public List<Paciente> buscarPorConsulta(Integer idPaciente) {
        return DaoPaciente.searchByIdPacienteStartsWith(idPaciente);
    }
}
