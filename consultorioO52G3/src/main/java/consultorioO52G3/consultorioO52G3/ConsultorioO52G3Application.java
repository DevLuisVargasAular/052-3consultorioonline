package consultorioO52G3.consultorioO52G3;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ConsultorioO52G3Application {

	public static void main(String[] args) {
		SpringApplication.run(ConsultorioO52G3Application.class, args);
	}

}
