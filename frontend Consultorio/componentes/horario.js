function buscarHorario() {
    let peticion = sendRequest('horario/listahorario', 'GET', "");
    let tabla = document.getElementById("lista-hora-medicos");
    peticion.onload = function() {
        let datos = peticion.response;
        datos.forEach(horario => {
            tabla.innerHTML += `
            <tr>
                <td>${horario.idHorario}</td>
                <td>${horario.medico.idMedico}</td>
                <td>${horario.InicioJornada}</td>
                <td>${horario.FinJornada}</td>
            </tr>
            
            `
            
        });

    }
}