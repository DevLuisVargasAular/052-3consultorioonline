function asignarCita() {
    let paciente = document.getElementById("txtIdPaciente").value;
    let medico = document.getElementById("txtIdMedico").value;
    let fechaSolicitud = document.getElementById("txtFechaSolicitud").value;
    let fechaDeLaCita = document.getElementById("txtFechaDeLaCita").value;
    let estadoAfiliacion = document.getElementById("txtEstadoAfiliacion").value;
    let estadoCita = document.getElementById("txtEstadoCita").value;
    let observacion = document.getElementById("txtObservacion").value;
    let formulaMedica = document.getElementById("txtFormulaMedica").value;
    let datos = {
        "paciente": {
            "idPaciente": paciente
        },
        "medico": {
            "idMedico": medico
        },
        "FechaSolicitud": fechaSolicitud,
        "FechaAsignada": fechaDeLaCita,
        "estadoAfiliacion": estadoAfiliacion,
        "estadoCita": estadoCita,
        "observacion": observacion,
        "formulaMedica": formulaMedica,
    };
    let peticion = sendRequest('Cita/insertarcita', 'POST', datos);
    peticion.onload=function(){
        alert("CITA ASIGNADA");
        location.reload();
    }
    return false;
}
