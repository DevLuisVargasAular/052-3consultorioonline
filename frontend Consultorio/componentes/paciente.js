function listarInfoPaciente() {
    let peticion = sendRequest('paciente/listapaciente', 'GET', '');
    let informacion = document.getElementById('listado-info-paciente');
    peticion.onload = function () {
        let datos = peticion.response;
        datos.forEach(paciente => {
            informacion.innerHTML += `
                <tr>
                    <td>${paciente.Identificacion}</td>
                    <td>${paciente.FechaExpedicion}</td>
                    <td>${paciente.Nombres}</td>
                    <td>${paciente.Apellidos}</td>
                    <td>${paciente.FechaNacimiento}</td>
                    <td>${paciente.EstadoAfiliacion}</td>
                </tr>
`
        });
    }

    peticion.onerror = function () {
        Swal.fire({
            position: 'top-end',
            icon: 'error',
            title: 'Error al cargar informacion de el paciente',
            showConfirmButton: false,
            timer: 1000
        })
    }

}