function mostrarDatosEditarCita(pacientee){
    let peticion = sendRequest('Cita/id?idCita='+pacientee, 'GET', "");
    peticion.onload = function(){
        let datos = peticion.response;
        let editarCita = datos;
        document.getElementById("input-id-cita").value = editarCita.idCita;
        document.getElementById("input-id-paciente").value = editarCita.paciente.idPaciente;
        document.getElementById("input-id-medico").value = editarCita.medico.idMedico;
        document.getElementById("input-fecha-solicitud").value = editarCita.FechaSolicitud;
        document.getElementById("input-fecha-asignada").value = editarCita.FechaAsignada;
        document.getElementById("input-observacion").value = editarCita.observacion;
        document.getElementById("input-estado-cita").value = editarCita.estadoCita;
        document.getElementById("input-formula").value = editarCita.formulaMedica;
        document.getElementById("input-afiliacion").value = editarCita.estadoAfiliacion;
    }

}
function editarLasCitas(){
    let idCita = document.getElementById("input-id-cita").value;
    let idPaciente = document.getElementById("input-id-paciente").value;
    let medico = document.getElementById("input-id-medico").value;
    let fechaSolicitud = document.getElementById("input-fecha-solicitud").value;
    let fechaDeLaCita = document.getElementById("input-fecha-asignada").value;
    let estadoAfiliacion = document.getElementById("input-afiliacion").value;
    let estadoCita = document.getElementById("input-estado-cita").value;
    let observacion = document.getElementById("input-observacion").value;
    let formulaMedica = document.getElementById("input-formula").value;
    let data = {};
    console.log(idPaciente);
    if (idCita.length > 0){
        data = {
            "idCita": idCita,
            "paciente": {
                "idPaciente": idPaciente
            },
            "medico": {
                "idMedico": medico
            },
            "FechaSolicitud": fechaSolicitud,
            "FechaAsignada": fechaDeLaCita,
            "estadoAfiliacion": estadoAfiliacion,
            "estadoCita": estadoCita,
            "observacion": observacion,
            "formulaMedica": formulaMedica,
        };
        console.log(data);
        let peticion = sendRequest('Cita/actualizarcita', 'PUT', data);
        peticion.onload=function(){
            alert("CITA ACTUALIZADA");
        }
    }
}

function buscarCitaPaciente() {
    let buscador = document.getElementById("buscador").value;
    let nombre = document.getElementById("identificacionPaciente").value;
    let peticion = sendRequest('Cita/buscar?idPaciente='+buscador, 'GET', "");
    let tabla = document.getElementById("lista-citas");
    tabla.innerHTML="";
    peticion.onload = function() {
        let datos = peticion.response;
        console.log(datos);
        datos.forEach(citas => {
            tabla.innerHTML += `
            <tr>
                <td>${citas.idCita}</td>
                <td>${citas.paciente.idPaciente}</td>
                <td>${citas.medico.idMedico}</td>
                <td>${citas.FechaSolicitud}</td>
                <td>${citas.FechaAsignada}</td>
                <td>${citas.observacion}</td>
                <td>${citas.estadoCita}</td>
                <td>${citas.formulaMedica}</td>
                <td>${citas.estadoAfiliacion}</td>
                <td>
                <button type="button" onclick="mostrarDatosEditarCita('${citas.idCita}')" class="btn btn-success">Editar</button>
                </td>
                
            </tr>
            
            `
            
        });

    }
}
