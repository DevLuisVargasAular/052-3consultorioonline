function buscarMedico() {
    let buscador = document.getElementById("buscador").value;
    let nombre = document.getElementById("identificacionMedico").value;
    let peticion = sendRequest('Medico/query?identificacion='+buscador, 'GET', "");
    let tabla = document.getElementById("lista-medicos");
    peticion.onload = function() {
        let datos = peticion.response;
        console.log(datos);
        datos.forEach(medico => {
            tabla.innerHTML += `
            <tr>
                <td>${medico.idMedico}</td>
                <td>${medico.identificacion}</td>
                <td>${medico.nombres}</td>
                <td>${medico.apellidos}</td>
                <td>${medico.especialidad}</td>
                <td>${medico.numero_Contacto}</td>
            </tr>
            
            `
            
        });

    }
}